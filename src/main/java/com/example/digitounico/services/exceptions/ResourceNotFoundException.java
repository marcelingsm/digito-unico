package com.example.digitounico.services.exceptions;

public class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(String id){
        super("Resource not found. Id: " + id);
    }
}
