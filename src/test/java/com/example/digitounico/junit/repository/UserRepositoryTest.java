package com.example.digitounico.junit.repository;

import com.example.digitounico.entities.CalcDigit;
import com.example.digitounico.entities.User;
import com.example.digitounico.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserRepositoryTest {

    @Test
    void insertUser() {
        UserRepository repo = new UserRepository();
        User user = new User("123","Pedro","pedro@email");
        Boolean resInsert = repo.insertUser(user);
        assertTrue(resInsert);
    }

    @Test
    void existIdNotInsertUser() {
        UserRepository repo = new UserRepository();
        User user = new User("123","Pedro","pedro@email");
        Boolean resInsert = repo.insertUser(user);
        assertTrue(resInsert);
        User user2 = new User("123","Marcelo","Marcelo@email");
        Boolean resNotInsert = repo.insertUser(user2);
        assertFalse(resNotInsert);
    }

    @Test
    void deleteById() {
        UserRepository repo = new UserRepository();
        User user = new User("123","Pedro","pedro@email");
        repo.insertUser(user);
        boolean resDelete = repo.deleteById("123");
        assertTrue(resDelete);
    }

    @Test
    void notDelete() {
        UserRepository repo = new UserRepository();
        User user = new User("123","Pedro","pedro@email");
        repo.insertUser(user);
        boolean resDelete = repo.deleteById("444");
        assertFalse(resDelete);
    }

    @Test
    void insertDigitUser() {
        UserRepository repo = new UserRepository();
        User user = new User("123","Pedro","pedro@email");
        repo.insertUser(user);
        CalcDigit calc  = new CalcDigit("9875", 1, 2, Instant.now());
        boolean resInsertCalc = repo.insertDigitUser(user.getId(),calc);
        assertTrue(resInsertCalc);
    }

    @Test
    void findAll() {
        UserRepository repo = new UserRepository();
        List<User> users = repo.findAll();
        assertEquals(0, users.size());
    }

    @Test
    void findAllMoreSize() {
        UserRepository repo = new UserRepository();
        User user = new User("123","Pedro","pedro@email");
        User user1 = new User("222","Rodrigo","rodrigo@email");
        User user2 = new User("333","Paulo","paulo@email");
        repo.insertUser(user);
        repo.insertUser(user1);
        repo.insertUser(user2);
        List<User> users = repo.findAll();
        assertEquals(3, users.size());
    }

    @Test
    void findById() {
        UserRepository repo = new UserRepository();
        User user = new User("123","Pedro","pedro@email");
        repo.insertUser(user);
        User userRepo = repo.findById("123");
        assertSame(user,userRepo);
    }

    @Test
    void notFoundFindById() {
        UserRepository repo = new UserRepository();
        User user = new User("123","Pedro","pedro@email");
        repo.insertUser(user);
        User ress = repo.findById("777");
        assertNull(ress);
    }

    @Test
    void update() {
        UserRepository repo = new UserRepository();
        User user = new User("123","Pedro","pedro@email");
        repo.insertUser(user);
        User userNewData = new User("123","Paulo","paulo@email");
        boolean resUpdate = repo.update(userNewData);
        assertTrue(resUpdate);
    }

    @Test
    void notUpdateIdNotExiste() {
        UserRepository repo = new UserRepository();
        User user = new User("123","Pedro","pedro@email");
        repo.insertUser(user);
        User userNewData = new User("222","Paulo","paulo@email");
        boolean resUpdate = repo.update(userNewData);
        assertFalse(resUpdate);
    }
}