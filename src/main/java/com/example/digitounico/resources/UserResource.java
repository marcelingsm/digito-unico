package com.example.digitounico.resources;

import com.example.digitounico.dto.UserDTO;
import com.example.digitounico.entities.CalcDigit;
import com.example.digitounico.entities.User;
import com.example.digitounico.services.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*")
@Api(value = "API REST CRUD user")
@RestController
@RequestMapping(value = "/users")
public class UserResource {

    @Autowired
    private UserService service;

    @ApiOperation(value = "Return list users")
    @GetMapping
    public ResponseEntity<List<UserDTO>> findAll() {
        List<User> userList = service.findAll();
        List<UserDTO> userDTOList = userList
                .stream()
                .map(user -> new UserDTO(user))
                .collect(Collectors.toList());
        return ResponseEntity.ok().body(userDTOList);
    }

    @ApiOperation(value = "Return users by id")
    @GetMapping(value = "/{id}")
    public ResponseEntity<User> findById(@PathVariable String id) {
        User obj = service.findById(id);
        return ResponseEntity.ok().body(obj);
    }

    @ApiOperation(value = "Calculates single digit and return int")
    @GetMapping(value = "/{id}/consulta/{n}")
    public ResponseEntity<Integer> calc(@PathVariable String id,@PathVariable String n) {
        int res = service.insertCalcDigit(id, n, 1);
        return ResponseEntity.ok().body(res);
    }

    @ApiOperation(value = "Calculates single digit concatenated and return int")
    @GetMapping(value = "/{id}/consulta/{n}/{k}")
    public ResponseEntity<Integer> calcConcatenate(
            @PathVariable String id,
            @PathVariable String n,
            @PathVariable int k) {
        int res = service.insertCalcDigit(id, n, k);
        return ResponseEntity.ok().body(res);
    }

    @ApiOperation(value = "Return query list by id")
    @GetMapping(value = "/{id}/consulta")
    public ResponseEntity<List<CalcDigit>> findDigitsById(@PathVariable String id) {
        User obj = service.findById(id);
        return ResponseEntity.ok().body(obj.getCalcDigits());
    }

    @ApiOperation(value = "User insert and return URI in Header")
    @PostMapping
    public ResponseEntity<Void> insert(@RequestBody UserDTO objDTO) {
        User obj = service.dTOToUser(objDTO);
        service.insert(obj);
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(obj.getId())
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    @ApiOperation(value = "User update")
    @PutMapping(value = "/{id}")
    public ResponseEntity<Void> update(@PathVariable String id, @RequestBody User obj) {
        service.update(id, obj);
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "User delete by id")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable String id) {
      service.delete(id);
      return ResponseEntity.noContent().build();
    }
}
