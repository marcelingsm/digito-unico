package com.example.digitounico.resources.exceptions;

import com.example.digitounico.services.exceptions.ResourceLargeNumberConcatenation;
import com.example.digitounico.services.exceptions.ResourceNotConvertNumber;
import com.example.digitounico.services.exceptions.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;

@ControllerAdvice
public class ResourceExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<StandardError> resourceNotFound(ResourceNotFoundException e, HttpServletRequest request){
        String error = "Resource not found";
        HttpStatus status = HttpStatus.NOT_FOUND;
        StandardError err = new StandardError(Instant.now(), status.value(), error,e.getMessage() ,request.getRequestURI());
        return ResponseEntity.status(status).body(err);
    }

    @ExceptionHandler(ResourceNotConvertNumber.class)
    public ResponseEntity<StandardError> convertNumber(ResourceNotConvertNumber e, HttpServletRequest request){
        String error = "Could not convert to number";
        HttpStatus status = HttpStatus.BAD_REQUEST;
        StandardError err = new StandardError(Instant.now(), status.value(), error,e.getMessage() ,request.getRequestURI());
        return ResponseEntity.status(status).body(err);
    }

    @ExceptionHandler(ResourceLargeNumberConcatenation.class)
    public ResponseEntity<StandardError> convertNumber(ResourceLargeNumberConcatenation e, HttpServletRequest request){
        String error = "The number of concatenations is too large";
        HttpStatus status = HttpStatus.BAD_REQUEST;
        StandardError err = new StandardError(Instant.now(), status.value(), error,e.getMessage() ,request.getRequestURI());
        return ResponseEntity.status(status).body(err);
    }
}
