package com.example.digitounico.cache;

import com.example.digitounico.entities.CalcDigit;
import org.springframework.stereotype.Repository;


import java.util.ArrayList;
import java.util.List;

@Repository
public class CalcDigitCache implements ICalcDigitCache{

    List<CalcDigit> calcDigitList = new ArrayList<>();

    @Override
    public Boolean insertCalcDigit(CalcDigit obj) {
        try {
            int SIZE_BUFF = 10;
            calcDigitList.add(obj);
            if(calcDigitList.size() > SIZE_BUFF) {
                calcDigitList.remove(0);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public List<CalcDigit> findAll() {
        return calcDigitList;
    }

    @Override
    public CalcDigit findOne(CalcDigit obj) {
        return calcDigitList
                .stream()
                .filter(x -> x.getConcatenation().equals(obj.getConcatenation()) && x.getNumberString().equals(obj.getNumberString()))
                .findFirst()
                .orElse(null);
    }
}
