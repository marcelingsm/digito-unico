package com.example.digitounico.resources;

import com.example.digitounico.entities.CalcDigit;
import com.example.digitounico.services.CalcDigitService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "Api cansulta Dígito Único")
@RestController
@RequestMapping(value = "/consulta")
public class CalcDigitResource {

    @Autowired
    private CalcDigitService service;

    @ApiOperation(value = "Return Cache Api")
    @GetMapping(value = "/cache")
    public ResponseEntity<List<CalcDigit>> cache() {
        return ResponseEntity.ok().body(service.cacheList());
    }

    @ApiOperation(value = "Calculates single digit and return int")
    @GetMapping(value = "/{n}")
    public ResponseEntity<Integer> calc(@PathVariable String n) {
        int result = service.calcUniqueDigit(n,1);
        return  ResponseEntity.ok().body(result);
    }

    @ApiOperation(value = "Calculates single digit concatenated and return int")
    @GetMapping(value = "/{n}/{k}")
    public ResponseEntity<Integer> calcConcatenate(@PathVariable String n, @PathVariable int k) {
        int result = service.calcUniqueDigit(n, k);
        return ResponseEntity.ok().body(result);

    }
}
