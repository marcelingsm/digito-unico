package com.example.digitounico.services;

import com.example.digitounico.dto.UserDTO;
import com.example.digitounico.entities.CalcDigit;
import com.example.digitounico.entities.User;

import com.example.digitounico.repository.UserRepository;

import com.example.digitounico.services.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository repo;
    @Autowired
    private CalcDigitService serviceCalcDigit;

    public List<User> findAll() {
        return repo.findAll();
    }

    public User findById(String id) {
        User obj = repo.findById(id);
        if(obj == null){
           throw new ResourceNotFoundException(id);
        }
        return obj;
    }

    public Boolean insert(User user) {
        return repo.insertUser(user);
    }

    public User dTOToUser(UserDTO userDTO) {
        return new User(userDTO.getId(), userDTO.getName(), userDTO.getEmail());
    }

    public Boolean delete(String id) {
        findById(id);
        return repo.deleteById(id);
    }

    public Boolean update(String id, User obj) {
        User newUser = findById(id);
        updateUser(newUser, obj);
        return repo.update(newUser);
    }

    private void updateUser(User newUser, User obj) {
        if(obj.getEmail() != null){
            newUser.setEmail(obj.getEmail());
        }
        if(obj.getName() != null){
            newUser.setName(obj.getName());
        }
    }

    public int insertCalcDigit(String id, String numberString, int concatenation) {
        findById(id);
        int result = serviceCalcDigit.calcUniqueDigit(numberString, concatenation);
        CalcDigit calcDigit = new CalcDigit(numberString, concatenation, result, Instant.now());
        repo.insertDigitUser(id, calcDigit);
        return result;
    }
}
