package com.example.digitounico.services;

import com.example.digitounico.cache.CalcDigitCache;
import com.example.digitounico.entities.CalcDigit;
import com.example.digitounico.services.exceptions.ResourceLargeNumberConcatenation;
import com.example.digitounico.services.exceptions.ResourceNotConvertNumber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Service
public class CalcDigitService {

    @Autowired
    private CalcDigitCache cache;

    public List<CalcDigit> cacheList() { return cache.findAll(); }

    public int calcUniqueDigit(String numberString, int concatenation) {
        if(concatenation < 1 || concatenation > 100000) {
            throw new ResourceLargeNumberConcatenation("Concatenation");
        }
        CalcDigit calcDigit = new CalcDigit(numberString,concatenation,1);
        CalcDigit calcDigitInCache = cache.findOne(calcDigit);
        if(calcDigitInCache == null){
            int resultCalc = calcSingleDigit(concatenateDigits(numberString,concatenation));
            saveOnCache(new CalcDigit(numberString,concatenation,resultCalc, Instant.now()));
            return resultCalc;
        }
        return calcDigitInCache.getResult();
    }

    private String concatenateDigits(String numberString, int concatenation) {
        String result = "";
        for (int i = 0; i < concatenation; i++) {
            result = result.concat(numberString);
        }
        return result;
    }

    private void saveOnCache(CalcDigit obj){
        cache.insertCalcDigit(obj);
    }

    private int calcSingleDigit(String numberString) {
       try {
           char[] charArray = numberString.toCharArray();
           List<Long> longArray = new ArrayList<>();
           for(char i : charArray){
               longArray.add(Long.parseLong(String.valueOf(i)));
           }
           Long sum = longArray.stream().reduce(0L, Long::sum);
           if(sum >= 10){
               return calcSingleDigit(Long.toString(sum));
           }
           else {
               return Integer.parseInt(String.valueOf(sum));
           }
       }catch (NumberFormatException e) {
           throw new ResourceNotConvertNumber(e.getMessage());
       }
    }
}
