package com.example.digitounico.cache;

import com.example.digitounico.entities.CalcDigit;

import java.util.List;

public interface ICalcDigitCache {
    Boolean insertCalcDigit(CalcDigit obj);
    List<CalcDigit> findAll();
    CalcDigit findOne(CalcDigit obj);
}
