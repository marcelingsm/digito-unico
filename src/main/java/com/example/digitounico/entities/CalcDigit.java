package com.example.digitounico.entities;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.time.Instant;

public class CalcDigit implements Serializable {
    private String numberString;
    private Integer concatenation;
    private Integer result;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'", timezone = "GMT")
    private Instant onDate;

    public CalcDigit(String numberString, Integer concatenation, Integer result, Instant onDate) {
        this.concatenation = concatenation;
        this.numberString = numberString;
        this.result = result;
        this.onDate = onDate;
    }

    public CalcDigit(String numberString, Integer concatenation, Integer result) {
        this.concatenation = concatenation;
        this.numberString = numberString;
        this.result = result;
    }

    public Integer getResult() {
        return result;
    }

    public Integer getConcatenation() {
        return concatenation;
    }

    public void setConcatenation(Integer concatenation) {
        this.concatenation = concatenation;
    }

    public String getNumberString() {
        return numberString;
    }

    public void setNumberString(String numberString) {
        this.numberString = numberString;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public Instant getOnDate() {
        return onDate;
    }

    public void setOnDate(Instant onDate) {
        this.onDate = onDate;
    }
}
