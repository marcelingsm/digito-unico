package com.example.digitounico.junit.cache;

import com.example.digitounico.cache.CalcDigitCache;
import com.example.digitounico.entities.CalcDigit;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CalcDigitCacheTest {

    CalcDigitCache cache = new CalcDigitCache();
    @Test
    void insertCalcDigit() {

        CalcDigit calcDigit = new CalcDigit("12345", 1, 3, Instant.now());
        boolean res = cache.insertCalcDigit(calcDigit);
        assertTrue(res);
    }

    @Test
    void insertCalcDigitMoreItens() {
        int insertNumber = 15;
        for(int i = 0; i<= insertNumber; i++) {
            CalcDigit calcDigit = new CalcDigit("12345", 1, 3, Instant.now());
            cache.insertCalcDigit(calcDigit);
        }
        List<CalcDigit> calcDigitList = cache.findAll();
        assertTrue(calcDigitList.size() == 10);
    }

    @Test
    void findAll() {
        CalcDigit calcDigit = new CalcDigit("12345", 1, 3, Instant.now());
        cache.insertCalcDigit(calcDigit);
        List<CalcDigit> calcDigitList = cache.findAll();
        assertTrue(calcDigitList.size() > 0);
    }

    @Test
    void findOne() {
        CalcDigit calcDigit = new CalcDigit("12345", 1, 3, Instant.now());
        cache.insertCalcDigit(calcDigit);
        CalcDigit calcDigitFind = cache.findOne(calcDigit);
        assertNotNull(calcDigitFind);
    }

    @Test
    void findOneNotFound() {
        CalcDigit calcDigit = new CalcDigit("12345", 1, 3, Instant.now());
        cache.insertCalcDigit(calcDigit);
        CalcDigit newCalcDigit = new CalcDigit("1548", 1, 3, Instant.now());
        CalcDigit calcDigitFind = cache.findOne(newCalcDigit);
        assertNull(calcDigitFind);
    }
}