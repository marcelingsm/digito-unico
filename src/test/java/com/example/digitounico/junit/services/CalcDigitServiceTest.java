package com.example.digitounico.junit.services;

import com.example.digitounico.cache.CalcDigitCache;
import com.example.digitounico.entities.CalcDigit;
import com.example.digitounico.services.CalcDigitService;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@RunWith(SpringRunner.class)
class CalcDigitServiceTest {

    @Autowired
    private CalcDigitCache cache;

    @Autowired
    private CalcDigitService service;

    @Before
    public void startCache() {
        int insertNumber = 15;
        for(int i = 0; i<= insertNumber; i++) {
            CalcDigit calcDigit = new CalcDigit("12345", i, 3, Instant.now());
            cache.insertCalcDigit(calcDigit);
        }
    }

    @Test
    void cacheList() {
        List<CalcDigit> calcDigitList = service.cacheList();
        assertTrue(calcDigitList.size() <= 10);
        assertTrue(calcDigitList.size() > 0);
    }

    @Test
    void calcUniqueDigitConcatenation() {
        int result = service.calcUniqueDigit("9", 1000);
        assertEquals(result, 9);
    }

    @Test
    void calcUniqueDigit() {
        int result = service.calcUniqueDigit("9875", 4);
        assertEquals(result, 8);
        int resultNoConcatenate = service.calcUniqueDigit("9875987598759875", 1);
        assertEquals(resultNoConcatenate, 8);
    }

    @Test
    void calcUniqueDigitOther() {
        int result = service.calcUniqueDigit("9875",1);
        assertEquals(result, 2);
    }

}