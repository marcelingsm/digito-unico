package com.example.digitounico.services.exceptions;

public class ResourceNotConvertNumber extends RuntimeException{
    public ResourceNotConvertNumber(String msg){
        super(msg);
    }
}
