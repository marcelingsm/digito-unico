package com.example.digitounico.services.exceptions;

public class ResourceLargeNumberConcatenation extends RuntimeException{
    public ResourceLargeNumberConcatenation(String msg){
        super(msg);
    }
}
