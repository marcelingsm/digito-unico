package com.example.digitounico.junit.services;


import com.example.digitounico.entities.User;
import com.example.digitounico.services.UserService;

import com.example.digitounico.services.exceptions.ResourceNotFoundException;
import org.junit.Before;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceTests {

    @Autowired
    UserService userService;

    @Before
    public void initInsert() {
        User user = new User("123", "Marcelo","marcelo@email.com");
        User user2 = new User("222", "Diana","diana@email.com");
        User user3= new User("333", "Rodrigo","rodrigo@email.com");
        userService.insert(user);
        userService.insert(user2);
        userService.insert(user3);
    }

    @Test
    public void testNotInsert() {
        User user = new User("123", "Marcelo","marcelo@email.com");
        userService.insert(user);
        Boolean resInsert = userService.insert(user);
        assertFalse(resInsert);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void delete() {
        Boolean resDelete = userService.delete("123");
        assertTrue(resDelete);
        userService.findById("123");
    }

    @Test(expected = ResourceNotFoundException.class)
    public void updateNotFoundId() {
        userService.update("0000", new User());
    }

    @Test
    public void update() {
        User user = new User("678", "Marcelo","marcelo@email.com");
        userService.insert(user);
        User newUser = new User(null, "Rodrigo","rodrigo@email.com");
        Boolean resUpdate = userService.update("678", newUser);
        assertTrue(resUpdate);
        User userRepo = userService.findById("678");
        assertEquals(userRepo.getName(), newUser.getName());
        assertEquals(userRepo.getEmail(), newUser.getEmail());
    }

    @Test
    public void testFindAll() {
        User user = new User("123", "Marcelo","marcelo@email.com");
        User newUser = new User("222", "Rodrigo","rodrigo@email.com");
        userService.insert(user);
        userService.insert(newUser);
        List<User> userList= userService.findAll();
        assertTrue(userList.size() > 0);
    }

    @Test
    public void testFindById() {
        User userFind = userService.findById("333");
        assertNotNull(userFind);
    }
}
