package com.example.digitounico.repository;

import com.example.digitounico.entities.CalcDigit;
import com.example.digitounico.entities.User;

import java.util.List;

public interface IUserRepository {
    Boolean insertUser(User obj);
    Boolean insertDigitUser(String id, CalcDigit calcDigit);
    Boolean deleteById(String id);
    List<User> findAll();
    User findById(String id);
    Boolean update(User obj);
}
