## Dígito único
Sistema Dígito único serve para calcular através de uma String (representando um inteiro) a soma dos seus dígitos até que reste apenas um único digito. Exemplo:
```bash
digito_unico(9875) 9 + 8 + 7 + 5 = 29
digito_unico(29) 2 + 9 = 11
digito_unico(11) 1 + 1 = 2
digito_unico(2) = 2
```
Dado dois números n e k, P deverá ser criado da concatenação da string
n * k.
Exemplo:

```bash
n=9875 e k=4 então p = 9875 9875 9875 9875
digitoUnico(9875987598759875)
5 + 7 + 8 + 9 + 5 + 7 + 8 + 9 + 5 + 7 + 8 + 9 + 5 + 7 + 8 + 9 = 116
digitoUnico(116) 1 + 1 + 6 = 8
digitoUnico(8)
```
Inserindo na consulta um Id de uma usuário ja existente, essa consulta sera salva no histórico de consultas do usuário.  
### Cache
Foi implementado um cache que salva (em memória) as 10 últimas consultas de dígito único realizadas, sendo essas consultas feitas por um usuário ou não.
## Tecnologia
- Java 8
- Spring Boot 2.5.3
- Maven 4.0.0
- Junit 4.12
- Swagger 2.7.0

## API

A API foi documentada através do Swagger. Para ter acesso à documentação, basta rodar o projeto e acessar:
```bash
http://localhost:8080/swagger-ui.html
```
*se estiver rodando o projeto localmente e na porta 8080.

Você pode tambem acessar o arquivo disponibilizado na pasta raiz do projeto. 
```bash
swagger.yaml
```
 
## Testes

### Testes unitários
Foram implementados testes unitários para garantir a integridade do sistema. Para rodar os testes basta acessar o caminho: 
```bash
src/test/java/com/example/digitounico/junit
```
em seguida execute os .java terminados em test.
### PostMan
Para testar os endPoints da aplicação foram criados testes manuais utilizando o PostMan. No diretório raiz da aplicação contem um arquivo chamado:
```bash
postman_collection.json
```
você pode importar na collections do PostMan. Caso haja dúvidas acesse o Link para mais informações: 
https://learning.postman.com/docs/getting-started/importing-and-exporting-data/#exporting-postman-data
## Observações
- Foi implementado também, alguns tratamentos de exceptions para obtermos uma resposta de erro mais precisa.
