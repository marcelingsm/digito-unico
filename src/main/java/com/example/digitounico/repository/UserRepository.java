package com.example.digitounico.repository;

import com.example.digitounico.entities.CalcDigit;
import com.example.digitounico.entities.User;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;

import java.util.List;

@Repository
public class UserRepository implements IUserRepository {

    List<User> userList = new ArrayList<>();

    @Override
    public Boolean insertUser(User obj) {
        try {
            User userExist = findById(obj.getId());
            if(userExist == null) {
                userList.add(obj);
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Boolean deleteById(String id) {
        boolean deleted = false;
        for(int i = 0; i < userList.size(); i++) {
            User p = userList.get(i);
            if(p.getId().equals(id)) {
                userList.remove(p);
                deleted = true;
                break;
            }
        }
        return deleted;
    }

    @Override
    public Boolean insertDigitUser(String id, CalcDigit calcDigit) {
        boolean inserted = false;
        for (User p : userList) {
            if (p.getId().equals(id)) {
                p.addCalcDigit(calcDigit);
                inserted = true;
                break;
            }
        }
        return inserted;
    }

    @Override
    public List<User> findAll() {
        return userList;
    }

    @Override
    public User findById(String id) {
        return userList
                .stream()
                .filter(x -> x.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Boolean update(User obj) {
        boolean updated = false;
        for (User p : userList) {
            if (p.getId().equals(obj.getId())) {
                p.setEmail(obj.getEmail());
                p.setName(obj.getName());
                updated = true;
                break;
            }
        }
        return updated;
    }
}
